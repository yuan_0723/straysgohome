package yvonneh.com.straysgohome;

import android.*;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static android.R.attr.x;
import static yvonneh.com.straysgohome.DataManager.PERMISSION_REQUEST_CODE;

/**
 * Created by User on 2017/3/19.
 */

public class ShelterMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int MY_REQUEST_CODE = 0;
    private static final int REQUEST_CODE_RESOLUTION = 1;
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private final static String TAG = "MainActivity";

    private GoogleApiClient googleApiClient;
    private Location location;
    Address shelterAddressObject;

    String shelterAddress;
    String shelterName;

    private GoogleMap shelterGoogleMap;
    SupportMapFragment mapViewFragment;

    TextView distanceTextView;
    Toolbar shelterMapToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shelter_map);

        // 請求定位權限
        askPermissions();

        // 取得上一頁Intent夾帶的收容所住址
        shelterAddress = getIntent().getStringExtra("shelterAddress");
        shelterName = getIntent().getStringExtra("shelterName");

        // 設定toolbar
        setupToolbar();

        mapViewFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.shelterMapBigView);
        mapViewFragment.getMapAsync(this);
        distanceTextView = (TextView) findViewById(R.id.distanceTextView);
    }


    // 設定Toolbar的方法
    private void setupToolbar() {
        shelterMapToolbar = (Toolbar) findViewById(R.id.shelterMapToolbar);
        shelterMapToolbar.setTitle("收容所地圖");
        setSupportActionBar(shelterMapToolbar);

        // 顯示ACTIONBAR上的返回鍵
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // 設定Map的方法
    private void settingMap() {

        // 獲取用以設定地圖的物件
        UiSettings mapSettings = shelterGoogleMap.getUiSettings();
        // 是否顯示交通流量
        shelterGoogleMap.setTrafficEnabled(false);

        // 是否呈現「自己位置的圖層」
        try {
            shelterGoogleMap.setMyLocationEnabled(true);
        } catch (SecurityException e) {
            Log.e("SecurityException",e.toString());
        }

        // 是否呈現地圖縮放按鈕
        mapSettings.setZoomControlsEnabled(true);
        // 是否顯示指北針
        mapSettings.setCompassEnabled(true);
        // 是否呈現「找到自己位置的按鈕」
        mapSettings.setMyLocationButtonEnabled(true);
        // 始否開啟捲動手勢
        mapSettings.setScrollGesturesEnabled(true);
        // 是否開啟縮放手勢
        mapSettings.setZoomGesturesEnabled(true);
        // 是否開啟傾斜手勢
        mapSettings.setTiltGesturesEnabled(true);
        // 是否開啟旋轉手勢
        mapSettings.setRotateGesturesEnabled(false);
    }

    // 將收容所地址轉為位置後再地圖上呈現Marker的方法
    private void turnShelterAddressToMarker(String shelterAddressStr) {

        // 增加新標記前，消除舊的Marker
        shelterGoogleMap.clear();
        Geocoder googleGeocoder = new Geocoder(this);
        // 解析地址後可能產生多筆位置資料，所以準備一個集合來承接接資訊;
        List<Address> geocoderAnalysisResultList = null;

        try {
            //Geocoder.getFromLocationName(地址字串, 需要回傳的筆數);
            geocoderAnalysisResultList = googleGeocoder.getFromLocationName(shelterAddressStr, 1);
        } catch (IOException ie) {
            Log.e("Geocoder",ie.toString());
        }

        if (geocoderAnalysisResultList == null || geocoderAnalysisResultList.isEmpty()) {
            // 不做任何事
        } else {

            // 取得地址分析後的第一筆位置資料
            shelterAddressObject = geocoderAnalysisResultList.get(0);
            // 從Address物件取出LatLng物件
            LatLng shelterLatLng = new LatLng(shelterAddressObject.getLatitude(), shelterAddressObject.getLongitude());
            // 從Address物件取出郵遞區號（需由地址GEOCODER分析才得以取得）
            String shelterAddressCode = shelterAddressObject.getPostalCode();
            String shelterMarkSnippet = shelterAddressCode + shelterAddressStr;

            // 將地址轉成位置後在地圖上打上對應的標記
            shelterGoogleMap.addMarker(new MarkerOptions().position(shelterLatLng)
                    .title(shelterName)
                    .snippet(shelterMarkSnippet));

            // 將地圖的鏡頭對焦在收容所的地址標記上
            CameraPosition mapCameraPosition = new CameraPosition.Builder().target(shelterLatLng)
                    .zoom(7)
                    .build();
            shelterGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(mapCameraPosition));
        }
    }


    private void askPermissions() {
        String[] permissions = {
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
        };

        Set<String> permissionsRequest = new HashSet<>();
        for (String permission : permissions) {
            int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                permissionsRequest.add(permission);
            }
        }

        if (!permissionsRequest.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    permissionsRequest.toArray(new String[permissionsRequest.size()]),
                    PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.shelterGoogleMap = googleMap;
        // 對地圖做相關設定
        settingMap();
        // 根據收容所地址將MARK呈現在地圖上
        turnShelterAddressToMarker(shelterAddress);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                String text = "";
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        text += permissions[i] + "\n";
                    }
                }
                if (!text.isEmpty()) {
                    text = "NO PERMISSION";
                    Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
    @Override
    // ACTIONBAR MENU 觸發事件覆寫
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (googleApiClient == null) {
            // 建立GOOGLE服務相關API
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)

                    // 加入事件間聽器
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)

                    // 建立服務物件
                    .build();
        }
        // 執行與GOOGLE服務的連線
        googleApiClient.connect();
    }

    @Override
    // 連上Google Play Services，系統呼叫此方法
    public void onConnected(Bundle bundle) {
        try {
            // 傳入剛剛建立的API服務，取得最近一次的位置資訊
            location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        } catch (SecurityException e) {
            Log.e("onConnected",e.toString());
        }
        // 位置請求細項設定
        LocationRequest locationRequest = LocationRequest.create()
                // 設置定位準確度
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                // 兩次定位間隔時間為10000毫秒
                .setInterval(10000)
                // 兩次定位之間最小距離間隔為1000公尺
                .setSmallestDisplacement(1000);
        // requestLocationUpdates(googleApiClient, 位置請求物件, 執行環境)
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    // 當連線Google Play Services發生暫停時，系統呼叫此方法
    public void onConnectionSuspended(int cause) {
        Toast.makeText(this, "Google Play Services連線發生暫停", Toast.LENGTH_SHORT).show();
    }

    @Override
    // 當連線Google Play Services失敗時，系統呼叫此方法
    public void onConnectionFailed(ConnectionResult result) {
        Toast.makeText(this, "連線Google Play Services失敗", Toast.LENGTH_SHORT).show();
        if (!result.hasResolution()) {
            GoogleApiAvailability availability = GoogleApiAvailability.getInstance();
            availability.getErrorDialog(this, result.getErrorCode(), REQUEST_RESOLVE_ERROR).show();
            return;
        }

        try {
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException sie) {
            Log.e(TAG, sie.toString());
        }
    }

    @Override
    // LocationListener：當請求到的位置發生改變時，系統呼叫此方法
    public void onLocationChanged(Location location) {
        getDistance();
        this.location = location;
    }

    // 計算距離
    public void getDistance() {

        // 確定以取得目前位置
        if (location == null || shelterAddress.isEmpty()) {
            return;
        }

        // 利用GEOCODER取得ADDRESS物件
        if (shelterAddressObject == null) {
            Toast.makeText(this, "無法找到收容所地址位置", Toast.LENGTH_SHORT).show();
            return;
        }

        float[] results = new float[1];

        // 計算自己位置與使用者輸入地點，此2點間的距離(公尺)，結果會存入results[0]
        // distanceBetween(自己的緯經度, 目的地的緯經度, 結果)
        Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                shelterAddressObject.getLatitude(), shelterAddressObject.getLongitude(), results);
        int distanceKM = (int)Math.floor(results[0]/1000);
        String distance = NumberFormat.getInstance().format(distanceKM);
        distanceTextView.setText("與收容所距離大約：" + distanceKM + "公里");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (googleApiClient != null) {
            // 與API的斷線
            googleApiClient.disconnect();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        askPermissions();
    }
}
