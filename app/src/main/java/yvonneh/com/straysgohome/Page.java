package yvonneh.com.straysgohome;

import android.support.v4.app.Fragment;
/**
 * Created by User on 2017/3/14.
 */

public class Page {
    // 宣告頁面物件會使用到的框架及頁面標題Property
    private Fragment contentFragment;
    private String pageTitle;

    // 建立建構子
    private Page(){
    }

    public Page(String pageTitle, Fragment contentFragment) {
        this.pageTitle = pageTitle;
        this.contentFragment = contentFragment;
    }

    // Property 的 Getter / Setter
    public Fragment getContentFragment() {
        return contentFragment;
    }

    public void setContentFragment(Fragment contentFragment) {
        this.contentFragment = contentFragment;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
}


