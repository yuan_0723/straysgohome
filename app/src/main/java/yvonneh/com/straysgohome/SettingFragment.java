package yvonneh.com.straysgohome;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import static android.content.Context.MODE_PRIVATE;
import static yvonneh.com.straysgohome.DataManager.DEFAULT_INDEX_SHOW;
import static yvonneh.com.straysgohome.DataManager.INDEX_SHOW_SHARED_PREFERENCE_KEY;
import static yvonneh.com.straysgohome.DataManager.SHARED_PREFERENCE_NAME;

/**
 * Created by User on 2017/3/14.
 */

public class SettingFragment extends Fragment {
    private DataManager dataManager;
    private SharedPreferences sharedPreferences;
    private int indexShow;
    private View settingView;
    private Spinner indexShowSpinner;
    private static RequestQueue imageLoadQueue;
    private int count = 0;
    RelativeLayout deleteCacheRelativeLayout, ratingAppRelativeLayout, appShareRelativeLayout,
                    reportRelativeLayout, adebeFBRelativeLayout, openDataWebRelativeLayout;

    // 建構子
    public SettingFragment() {
        dataManager = DataManager.shareInstance();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFERENCE_NAME,MODE_PRIVATE);

        // 載入首頁顯示的偏好設定
        loadSharedPreferences();

        imageLoadQueue = Volley.newRequestQueue(getActivity());

        settingView = inflater.inflate(R.layout.fragment_setting, container, false);
        indexShowSpinner = (Spinner) settingView.findViewById(R.id.indexShowSpinner);
        indexShowSpinner.setSelection(indexShow, true);
        // 將程式碼與物件做關聯
        findViews();
        // 啟用客製ActionBar
        setHasOptionsMenu(true);
        // 設置物件監聽事件
        setupListener();

        return settingView;
    }

    // 將程式碼與物件做關聯的方法
    private void findViews() {
        deleteCacheRelativeLayout = (RelativeLayout) settingView.findViewById(R.id.deleteCacheRelativeLayout);
        ratingAppRelativeLayout = (RelativeLayout) settingView.findViewById(R.id.ratingAppRelativeLayout);
        appShareRelativeLayout = (RelativeLayout) settingView.findViewById(R.id.appShareRelativeLayout);
        reportRelativeLayout = (RelativeLayout) settingView.findViewById(R.id.reportRelativeLayout);
        adebeFBRelativeLayout = (RelativeLayout) settingView.findViewById(R.id.adebeFBRelativeLayout);
        openDataWebRelativeLayout = (RelativeLayout) settingView.findViewById(R.id.openDataWebRelativeLayout);
    }

    // 讀取偏好設定的方法
    private void loadSharedPreferences() {
        indexShow = sharedPreferences.getInt(INDEX_SHOW_SHARED_PREFERENCE_KEY, DEFAULT_INDEX_SHOW);
    }

    // 將收藏的浪浪編號集合寫入偏好設定的方法
    private void saveIndexShowSetting() {
        sharedPreferences.edit().putInt(INDEX_SHOW_SHARED_PREFERENCE_KEY,indexShow).apply();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.setting_menu, menu);
    }
    // 設置監聽事件的方法
    private void setupListener() {
        // spinner事件處理

        indexShowSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // 儲存首頁設定到USERDEFAULT
                // 利用變數去控制，避免畫面建立的當下觸發SPINNER事件
                if (++count > 0) {
                    String indexSettingStr;
                    if (indexShowSpinner.getSelectedItemPosition() == 1) {
                        indexShow = 1;
                        indexSettingStr = "只為您呈現喵喵";
                    } else if (indexShowSpinner.getSelectedItemPosition() == 2) {
                        indexShow = 2;
                        indexSettingStr = "只為您呈現汪汪";
                    } else {
                        indexShow = 0;
                        indexSettingStr = "為您呈現所有類型";
                    }
                    saveIndexShowSetting();
                    Toast.makeText(getContext(), "首頁顯示設定已被更改，下次啟動時將"+indexSettingStr+"的領養資訊", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}

            });


        // 清除Volley緩存快取
        deleteCacheRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteCacheAlert deleteCacheAlert = new DeleteCacheAlert();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                deleteCacheAlert.show(fragmentManager, "deleteCache");
            }
        });

        // 評價APP
        ratingAppRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"連結到GOOGLEPLAY",Toast.LENGTH_LONG).show();
            }
        });

        // 分享APP
        appShareRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareAppText = "浪浪回家APP：一個愛的機會，認養代替購買\n\nApp Store：\nhttps://itunes.apple.com/tw/app/id1212078175";
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareAppText);
                shareIntent.setType("text/plain");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "請選擇要用來推廣「浪浪回家APP」的應用程式"));
            }
        });

        // 問題回報
        reportRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"0723yuan@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "APP問題回報：浪浪回家");
                intent.putExtra(Intent.EXTRA_TEXT, "手機品牌：\n手機型號：\nAndroid版本號：\n\n想要回報的事情：");
                intent.setType("message/rfc822");
                startActivity(Intent.createChooser(intent, "請選擇要用來發送E-Mail的應用程式"));
            }
        });

        // 阿呆比粉絲專業
        adebeFBRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 跳頁到阿呆比粉絲專業
                String adebbyFBUrlStr = "https://www.facebook.com/Adebby0409";
                Uri adebbyFBUrl = Uri.parse(adebbyFBUrlStr);
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, adebbyFBUrl);
                startActivity(launchBrowser);
            }
        });

        // 農委會開放資訊平台
        openDataWebRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String openDataWebUrlStr = "http://data.coa.gov.tw";
                Uri openDataWebUrl = Uri.parse(openDataWebUrlStr);
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, openDataWebUrl);
                startActivity(launchBrowser);
            }
        });
    }

    // 自訂刪除快取的警告視窗類別，並實作點擊介面
    public static class DeleteCacheAlert extends DialogFragment implements DialogInterface.OnClickListener {
        public DeleteCacheAlert() {
        }

        @Override
        // 建立警告視窗元件
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog deleteCacheAlert = new AlertDialog.Builder(getActivity())
                    .setIcon(R.drawable.dialog_icon)
                    .setTitle("注意！")
                    .setMessage("確定要清除緩存快取嗎？")
                    .setPositiveButton("確認", this)
                    .setNegativeButton("取消", this)
                    .create();
            return  deleteCacheAlert;
        }

        // 警告視窗的點擊事件
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    imageLoadQueue.getCache().clear();
//                    Toast.makeText(getContext(),"緩存清理完成",Toast.LENGTH_LONG).show();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.cancel();
                    break;
                default:
                    break;
            }
        }
    }
}
