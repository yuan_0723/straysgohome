package yvonneh.com.straysgohome;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import static yvonneh.com.straysgohome.DataManager.DEFAULT_FAVORITE_STRAY_IDS;
import static yvonneh.com.straysgohome.DataManager.FAVORITE_STRAY_IDS_PREFERENCE_KEY;
import static yvonneh.com.straysgohome.DataManager.SHARED_PREFERENCE_NAME;

/**
 * Created by User on 2017/3/18.
 */

public class SearchResultActivity extends AppCompatActivity {
    DataManager dataManager;
    Toolbar searchResultToolbar;
    RecyclerView searchResultRecyclerView;
    static SearchResultAdapter searchResultAdapter;
    static ArrayList<Stray> searchResultList;
    static SharedPreferences sharedPreferences;
    static HashSet<String> favoriteStrayIDs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serch_result);
        String[] searchFeature = getIntent().getExtras().getStringArray("searchFeature");
        dataManager = DataManager.shareInstance();
        searchResultRecyclerView = (RecyclerView) findViewById(R.id.searchResultRecyclerView);
        setupToolbar();
        sharedPreferences = getSharedPreferences(SHARED_PREFERENCE_NAME,MODE_PRIVATE);
        loadFavoriteStrayIDs();
        new SearchStrays().execute(searchFeature);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadFavoriteStrayIDs();
        if (searchResultAdapter != null) {
//            searchResultRecyclerView.setLayoutManager(new LinearLayoutManager(SearchResultActivity.this));
        }
    }

    // 設定Toolbar的方法
    private void setupToolbar() {
        searchResultToolbar = (Toolbar) findViewById(R.id.searchResultToolbar);
        searchResultToolbar.setTitle("搜尋結果");
        setSupportActionBar(searchResultToolbar);

        // 顯示ACTIONBAR上的返回鍵
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    // ACTIONBAR MENU 觸發事件覆寫
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    // 讀取偏好設定的方法
    private static void loadFavoriteStrayIDs() {
        favoriteStrayIDs = (HashSet<String>) sharedPreferences.getStringSet(FAVORITE_STRAY_IDS_PREFERENCE_KEY, DEFAULT_FAVORITE_STRAY_IDS);
    }
    // 將收藏的浪浪編號集合寫入偏好設定的方法(務必要加上.clear()，否則在重啟後會遺失數據)
    private static void saveFavoriteStrayIDs(HashSet<String> favoriteStrayIDs) {
        sharedPreferences.edit()
                .clear()
                .putStringSet(FAVORITE_STRAY_IDS_PREFERENCE_KEY,favoriteStrayIDs)
                .apply();
    }

    // 用來判斷要顯示的性別種類標籤的方法
    private int GetStrayGenderKindImage(String kindStr, String genderStr) {
        int genderKindImageResID;
        if (kindStr.equals("貓")) {
            if (genderStr.equals("F")) {
                genderKindImageResID = R.drawable.kind_catgirl;
            } else if (genderStr.equals("M")) {
                genderKindImageResID = R.drawable.kind_catboy;
            } else {
                genderKindImageResID = R.drawable.kind_catunknown;
            }
        } else {
            if (genderStr.equals("F")) {
                genderKindImageResID = R.drawable.kind_doggirl;
            } else if (genderStr.equals("M")) {
                genderKindImageResID = R.drawable.kind_dogboy;
            } else {
                genderKindImageResID = R.drawable.kind_dogunknown;
            }
        }
        return genderKindImageResID;
    }

    public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchResultViewHolder> {
        private LayoutInflater searchResultInflater;
        private RequestQueue imageLoadQueue;
        private ImageLoader volleyImageLoader;

        public SearchResultAdapter(Context context) {
            searchResultInflater = LayoutInflater.from(context);
            imageLoadQueue = Volley.newRequestQueue(context);
            volleyImageLoader = new ImageLoader(imageLoadQueue, new BitmapCacheManager());
            Log.e("SearchResultAdapter","SearchResultAdapter");
        }

        @Override
        public SearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new SearchResultAdapter.SearchResultViewHolder(searchResultInflater.inflate(R.layout.cardview_favorite, parent, false));
        }

        @Override
        public void onBindViewHolder(final SearchResultViewHolder holder, int position) {

            // 取得該CELL的Stray物件
            final  Stray stray = searchResultList.get(position);

            // 要顯示的字串處理
            holder.strayShelterNameTextView.setText(stray.getAnimalShelterName());
            holder.strayBodyTypeTextView.setText(dataManager.showUserAnimalBodyTypeStr(stray.getAnimalBodyType()));
            holder.strayAgeTextView.setText(dataManager.showUserAnimalAgeStr(stray.getAnimalAge()));
            holder.straySterilizationTextView.setText(dataManager.showUserAnimalSterilizationStr(stray.getAnimalSterilization()));
            holder.strayBacterinTextView.setText(dataManager.showUserAnimalBacterinStr(stray.getAnimalBacterin()));

            // 要顯示的性別種類標籤
            holder.strayGenderKindImage.setImageResource(GetStrayGenderKindImage(stray.getAnimalKind(),stray.getAnimalGender()));

            // 設定照片顯示
            if (stray.getAnimalPhoto().length() == 0) {
                int noPhotoImageID;
                if (stray.getAnimalKind().equals("貓")) {
                    noPhotoImageID = R.drawable.cat_no_image;
                } else {
                    noPhotoImageID = R.drawable.dog_no_image;
                }
                holder.strayPhotoImage.setImageResource(noPhotoImageID);
            } else {
                holder.strayPhotoImage.setImageUrl(stray.getAnimalPhoto(), volleyImageLoader);
            }

            // 判斷是否顯示隱藏收藏按鈕
            // 判斷流浪動物編號是否在USERDEFAULT收藏中，若有則顯示取消收藏按鈕
            if (favoriteStrayIDs.contains(stray.getAnimalID())) {
                Log.e("HEEEERRR","HEEERRR");
                holder.cancelFavoriteBtn.setVisibility(ImageButton.VISIBLE);
                holder.favoriteBtn.setVisibility(ImageButton.GONE);
            } else {
                Log.e("HEEEERRR","HEEERRR");
                holder.cancelFavoriteBtn.setVisibility(ImageButton.GONE);
                holder.favoriteBtn.setVisibility(ImageButton.VISIBLE);
            }

            // 設置按鈕點擊觸發事件
            holder.favoriteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 加入流浪動物編號到USERDEFAULT收藏
                    favoriteStrayIDs.add(stray.getAnimalID());
                    saveFavoriteStrayIDs(favoriteStrayIDs);

                    // !!!隱藏/顯示對應按鈕
                    v.setVisibility(ImageButton.GONE);
                    holder.cancelFavoriteBtn.setVisibility(View.VISIBLE);
                    loadFavoriteStrayIDs();
                }
            });
            holder.cancelFavoriteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 移除USERDEFAULT收藏的流浪動物編號
                    favoriteStrayIDs.remove(stray.getAnimalID());
                    saveFavoriteStrayIDs(favoriteStrayIDs);
                    // !!!隱藏/顯示對應按鈕
                    v.setVisibility(ImageButton.GONE);
                    holder.favoriteBtn.setVisibility(View.VISIBLE);
                    loadFavoriteStrayIDs();
                }
            });

            // 設置點擊觸發跳轉事件
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SearchResultActivity.this, StrayInfoActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("strayInfo", stray);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            // 檢查搜尋結果的List內是否有資料
            if (searchResultList != null) {
                return searchResultList.size();
            } else {
                return 0;
            }
        }

        class SearchResultViewHolder extends RecyclerView.ViewHolder {
            ImageView strayGenderKindImage;
            NetworkImageView strayPhotoImage;
            ImageButton favoriteBtn, cancelFavoriteBtn;
            TextView strayShelterNameTextView, strayBodyTypeTextView, strayAgeTextView, straySterilizationTextView, strayBacterinTextView;

            public SearchResultViewHolder(View itemView) {
                super(itemView);
                strayGenderKindImage = (ImageView) itemView.findViewById(R.id.strayGenderKindImage);
                strayPhotoImage = (NetworkImageView) itemView.findViewById(R.id.strayPhotoImage);
                favoriteBtn = (ImageButton) itemView.findViewById(R.id.favoriteBtn);
                cancelFavoriteBtn = (ImageButton) itemView.findViewById(R.id.cancelFavoriteBtn);
                strayShelterNameTextView = (TextView) itemView.findViewById(R.id.strayShelterNameTextView);
                strayBodyTypeTextView = (TextView) itemView.findViewById(R.id.strayBodyTypeTextView);
                strayAgeTextView = (TextView) itemView.findViewById(R.id.strayAgeTextView);
                straySterilizationTextView = (TextView) itemView.findViewById(R.id.straySterilizationTextView);
                strayBacterinTextView = (TextView) itemView.findViewById(R.id.strayBacterinTextView);
            }
        }
    }


    private class SearchStrays extends AsyncTask<String[], Integer, ArrayList<Stray>> {
        private ProgressDialog searchProgressDialog;
        List<Stray> allStraysList;
        ArrayList<Stray> tmpSearchResultList;

        @Override
        protected void onPreExecute() {
            allStraysList = dataManager.getAllStraysList();
            // 準備一個用來承接篩選結果的清單集合
            tmpSearchResultList = new ArrayList<>();
            // 顯示資料搜尋轉轉
            searchProgressDialog = new ProgressDialog(SearchResultActivity.this);
            searchProgressDialog.setMessage("浪浪搜尋中，請稍後...");
            searchProgressDialog.show();
        }

        @Override
        protected ArrayList<Stray> doInBackground(String[]... taskParams) {
            String[] searchFeature = taskParams[0];

            for(Iterator iterator = allStraysList.iterator(); iterator.hasNext();) {
                Stray tmpStray = (Stray) iterator.next();
                // 根據篩選條件篩選符合資格的Stray物件（[種類, 性別, 體型, 年紀, 毛色, 地區]）
                boolean isKindSame = true;
                boolean isGenderSame = true;
                boolean isBodySame = true;
                boolean isAgeSame = true;
                boolean isLocationSame = true;
                boolean isClourSame = false;

                // 種類
                if (!searchFeature[0].equals("都可以")) {
                    isKindSame = tmpStray.getAnimalKind().equals(searchKindToJSONStr(searchFeature[0]));
                }

                // 性別
                if (!searchFeature[1].equals("都可以")) {
                    isGenderSame = tmpStray.getAnimalGender().equals(searchGenderToJSONStr(searchFeature[1]));
                }

                // 體型
                if (!searchFeature[2].equals("都可以")) {
                    if (searchBodyToJSONStr(searchFeature[2]).equals("MINISMALL")) {
                        isBodySame = tmpStray.getAnimalBodyType().equals("MINI") || tmpStray.getAnimalBodyType().equals("SMALL");
                    } else {
                        isBodySame = tmpStray.getAnimalBodyType().equals(searchBodyToJSONStr(searchFeature[2]));
                    }
                }

                // 年紀
                if (!searchFeature[3].equals("都可以")) {
                    isAgeSame = tmpStray.getAnimalAge().equals(searchAgeToJSONStr(searchFeature[3]));
                }

                // 地區
                if (!searchFeature[5].equals("都可以")) {
                    isLocationSame = tmpStray.getAnimalShelterName().contains(searchFeature[5]);
                }

                // 毛色
                if (!searchFeature[4].equals("都可以")) {
                    String tmpStrayColor = tmpStray.getAnimalColour();

                    if (searchFeature[4].equals("米白色")) {
                        isClourSame = tmpStrayColor.contains("米") || tmpStrayColor.contains("白");
                    } else if (searchFeature[4].equals("橘黃色")) {
                        isClourSame = tmpStrayColor.contains("橘") || tmpStrayColor.contains("黃");
                    } else if (searchFeature[4].equals("棕色")) {
                        isClourSame = tmpStrayColor.contains("棕") || tmpStrayColor.contains("咖啡");
                    } else if (searchFeature[4].equals("黑色")) {
                        isClourSame = tmpStrayColor.contains("黑");
                    } else if (searchFeature[4].equals("虎斑")) {
                        isClourSame = tmpStrayColor.contains("虎");
                    } else if (searchFeature[4].equals("花色")) {
                        isClourSame = tmpStrayColor.contains("花");
                    }
                } else {
                    isClourSame = true;
                }

                // 篩選出符合所有條件的浪浪
                if (isKindSame && isGenderSame && isBodySame && isAgeSame && isLocationSame && isClourSame) {
                    tmpSearchResultList.add(tmpStray);
                }
            }
            return tmpSearchResultList;
        }

        @Override
        protected void onPostExecute(ArrayList<Stray> tmpSearchResult) {
            searchProgressDialog.dismiss();
            searchResultList = tmpSearchResult;
            // 設定轉接頭
            searchResultRecyclerView.setLayoutManager(new LinearLayoutManager(SearchResultActivity.this));
            searchResultAdapter = new SearchResultAdapter(SearchResultActivity.this);
            searchResultRecyclerView.setAdapter(new SearchResultAdapter(SearchResultActivity.this));
            if (searchResultList.size() == 0) {
                NoResultAlert noResultAlert = new NoResultAlert();
                FragmentManager fragmentManager = SearchResultActivity.this.getSupportFragmentManager();
                noResultAlert.show(fragmentManager, "noSearchResult");
            }
        }
    }

    private String searchKindToJSONStr(String searchKind) {
        if (searchKind.equals("喵喵貓")) {
            return "貓";
        } else {
            return "狗";
        }
    }

    private String searchGenderToJSONStr(String searchGender) {
        if (searchGender.equals("格格")) {
            return "F";
        } else {
            return "M";
        }
    }

    private String searchBodyToJSONStr(String searchBody) {
        if (searchBody.equals("小型")) {
            return "MINISMALL";
        } else if (searchBody.equals("中型")){
            return "MEDIUM";
        } else {
            return "BIG";
        }
    }

    private String searchAgeToJSONStr(String searchAge) {
        if (searchAge.equals("幼年")) {
            return "CHILD";
        } else {
            return "ADULT";
        }
    }

    // 自訂的的無搜尋結果的警告視窗類別，並實作點擊介面
    public static class NoResultAlert extends DialogFragment implements DialogInterface.OnClickListener {
        public NoResultAlert() {
        }

        @Override
        // 建立警告視窗元件
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog noResultAlert = new AlertDialog.Builder(getActivity())
                    .setIcon(R.drawable.dialog_icon)
                    .setTitle("沒有相關資料")
                    .setMessage("沒有搜尋到符合搜尋標準的浪浪資料，建議可以調整搜尋標準後再試試看～ ")
                    .setNeutralButton("我知道了",this)
                    .create();
            return  noResultAlert;
        }

        // 警告視窗的點擊事件
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_NEUTRAL:
                    getActivity().finish();
                    break;
                default:
                    break;
            }
        }
    }

}
