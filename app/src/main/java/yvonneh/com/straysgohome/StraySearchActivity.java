package yvonneh.com.straysgohome;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import java.util.List;

/**
 * Created by User on 2017/3/17.
 */

public class StraySearchActivity extends AppCompatActivity {



    // 會使用到的基本UI相關物件
    Spinner searchKindSpinner, searchGenderSpinner, searchBodySpinner, searchAgeSpinner,
            searchClourSpinner,searchLocationSpinner;
    Button starSearchBtn;
    Toolbar straySearchToolbar;
    DataManager dataManager;
    List<Stray> allStraysList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stray_search);
        findViews();
        setupClickListener();
        dataManager = DataManager.shareInstance();


    }

    private void findViews() {
        setupToolbar();
        searchKindSpinner = (Spinner) findViewById(R.id.searchKindSpinner);
        searchKindSpinner.setSelection(0,true);
        searchGenderSpinner = (Spinner) findViewById(R.id.searchGenderSpinner);
        searchGenderSpinner.setSelection(0,true);
        searchBodySpinner = (Spinner) findViewById(R.id.searchBodySpinner);
        searchBodySpinner.setSelection(0,true);
        searchAgeSpinner = (Spinner) findViewById(R.id.searchAgeSpinner);
        searchAgeSpinner.setSelection(0,true);
        searchClourSpinner = (Spinner) findViewById(R.id.searchClourSpinner);
        searchClourSpinner.setSelection(0,true);
        searchLocationSpinner = (Spinner) findViewById(R.id.searchLocationSpinner);
        searchLocationSpinner.setSelection(0,true);
        starSearchBtn = (Button) findViewById(R.id.starSearchBtn);
    }

    @Override
    // ACTIONBAR MENU 觸發事件覆寫
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // 設定Toolbar的方法
    private void setupToolbar() {
        straySearchToolbar = (Toolbar) findViewById(R.id.straySearchToolbar);
        straySearchToolbar.setTitle("搜尋浪浪");
        setSupportActionBar(straySearchToolbar);

        // 顯示ACTIONBAR上的返回鍵
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // 設定物件監聽的方法
    private void setupClickListener() {
        starSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 將要搜尋的特徵整理成字串集合帶到下一頁
                String[] searchFeature = { searchKindSpinner.getSelectedItem().toString(),
                        searchGenderSpinner.getSelectedItem().toString(),
                        searchBodySpinner.getSelectedItem().toString(),
                        searchAgeSpinner.getSelectedItem().toString(),
                        searchClourSpinner.getSelectedItem().toString(),
                        searchLocationSpinner.getSelectedItem().toString() };
                Intent intent = new Intent(StraySearchActivity.this, SearchResultActivity.class);
                intent.putExtra("searchFeature", searchFeature);
                startActivity(intent);
            }
        });
    }
}
