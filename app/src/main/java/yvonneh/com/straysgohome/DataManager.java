package yvonneh.com.straysgohome;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by User on 2017/3/14.
 */

public class DataManager {

    // 配合單例的靜態物件
    private  static  DataManager dataManager = new DataManager();

    // 流浪動物開放資料的Json網址
    public final static String STRAYS_JSON_URL = "http://data.coa.gov.tw/Service/OpenData/TransService.aspx?UnitId=QcbUEzN6E6DL";
    // JSON開放資料裡沒有細項的相關資料時要呈現的字串
    private final String UNKNOWN_DATA = "待確認";
    // 用以承接網路上下載下來的浪浪資料集合
    private List<Stray> allStraysList;

    // 偏好設定檔的名稱
    public final static String SHARED_PREFERENCE_NAME = "straysGoHomeSetting";
    // 首頁顯示的偏好設定KEY值
    public final static String INDEX_SHOW_SHARED_PREFERENCE_KEY = "indexShow";
    // 首頁顯示設定預設值
    public final static int DEFAULT_INDEX_SHOW = 0;
    // 收藏的浪浪編號的偏好設定KEY值
    public final static String FAVORITE_STRAY_IDS_PREFERENCE_KEY = "favoriteStrayIDs";
    // 預設收藏的浪浪編號空字串集合
    public final static Set<String> DEFAULT_FAVORITE_STRAY_IDS = new HashSet<String>();
    // 權限請求代碼
    public static final int PERMISSION_REQUEST_CODE = 1;


    private DataManager() {
    }

    public static DataManager shareInstance() {
        return dataManager;
    }

    public List<Stray> getAllStraysList() {
        return allStraysList;
    }

    public void setAllStraysList(List<Stray> allStraysList) {
        this.allStraysList = allStraysList;
    }

    // 將原始Json字串轉換成給使用者觀看的字串方法
    public String showUserAnimalKindStr(String animalKindStr) {
        String showUserAnimalKindStr;
        if (animalKindStr.equals("貓")) {
            showUserAnimalKindStr = "喵喵貓";
        } else if (animalKindStr.equals("狗")){
            showUserAnimalKindStr = "汪汪狗";
        } else {
            showUserAnimalKindStr = UNKNOWN_DATA;
        }
        return showUserAnimalKindStr;
    }
    public String showUserAnimalGenderStr(String animalGenderStr) {
        String showUserAnimalGenderStr;
        if (animalGenderStr.equals("F")) {
            showUserAnimalGenderStr = "格格";
        } else if (animalGenderStr.equals("M")){
            showUserAnimalGenderStr = "阿哥";
        } else {
            showUserAnimalGenderStr = UNKNOWN_DATA;
        }
        return showUserAnimalGenderStr;
    }
    public String showUserAnimalBodyTypeStr(String animalBodyTypeStr) {
        String showUserAnimalBodyTypeStr;
        if (animalBodyTypeStr.equals("MINI") || animalBodyTypeStr.equals("SMALL")) {
            showUserAnimalBodyTypeStr = "小型";
        } else if (animalBodyTypeStr.equals("MEDIUM")){
            showUserAnimalBodyTypeStr = "中型";
        } else if (animalBodyTypeStr.equals("BIG")){
            showUserAnimalBodyTypeStr = "大型";
        } else {
            showUserAnimalBodyTypeStr = UNKNOWN_DATA;
        }
        return showUserAnimalBodyTypeStr;
    }
    public String showUserAnimalAgeStr(String animalAgeStr) {
        String showUserAnimalAgeStr;
        if (animalAgeStr.equals("CHILD")) {
            showUserAnimalAgeStr = "幼年";
        } else if (animalAgeStr.equals("ADULT")){
            showUserAnimalAgeStr = "成年";
        } else {
            showUserAnimalAgeStr = UNKNOWN_DATA;
        }
        return showUserAnimalAgeStr;
    }
    public String showUserAnimalSterilizationStr(String animalSterilizationStr) {
        String showUserAnimalSterilizationStr;
        if (animalSterilizationStr.equals("T") || animalSterilizationStr.equals("Y")) {
            showUserAnimalSterilizationStr = "Yes";
        } else if (animalSterilizationStr.equals("N") || animalSterilizationStr.equals("F")){
            showUserAnimalSterilizationStr = "No";
        } else {
            showUserAnimalSterilizationStr = UNKNOWN_DATA;
        }
        return showUserAnimalSterilizationStr;
    }
    public String showUserAnimalBacterinStr(String animalBacterinStr) {
        String showUserAnimalBacterinStr;
        if (animalBacterinStr.equals("T") || animalBacterinStr.equals("Y")) {
            showUserAnimalBacterinStr = "Yes";
        } else if (animalBacterinStr.equals("N") || animalBacterinStr.equals("F")){
            showUserAnimalBacterinStr = "No";
        } else {
            showUserAnimalBacterinStr = UNKNOWN_DATA;
        }
        return showUserAnimalBacterinStr;
    }
    public String showUserAnimalNoteStr(String animalRemarkStr, String animalCaptionStr) {
        String showUserAnimalNoteStr;
        if (animalCaptionStr.length() == 0) {
            showUserAnimalNoteStr = animalRemarkStr;
        } else {
            showUserAnimalNoteStr = animalRemarkStr + "(" + animalCaptionStr + ")";
        }
        return showUserAnimalNoteStr;
    }
    public String showUserAnimalShelterPhoneStr(String animalShelterStr, String animalShelterPhoneStr) {
        String showUserAnimalShelterPhoneStr;
        if (animalShelterStr.equals("臺北市動物之家")) {
            showUserAnimalShelterPhoneStr = "02-87913254";
        } else if (animalShelterStr.equals("桃園市動物保護教育園區")) {
            showUserAnimalShelterPhoneStr = "03-4861760";
        } else if (animalShelterStr.equals("嘉義縣流浪犬中途之家")) {
            showUserAnimalShelterPhoneStr = "05-2724721";
        } else if (animalShelterStr.equals("屏東縣流浪動物收容所")) {
            showUserAnimalShelterPhoneStr = "08-7701094";
        } else {
            showUserAnimalShelterPhoneStr = animalShelterPhoneStr;
        }
        return showUserAnimalShelterPhoneStr;
    }
    public String showUserAnimalShelterAddress(String animalShelterStr, String animalShelterAddressStr) {
        String showUserAnimalShelterAddressStr;
        if (animalShelterStr.equals("彰化縣流浪狗中途之家")) {
            showUserAnimalShelterAddressStr = "彰化縣芬園鄉大彰路一段875巷";
        } else if (animalShelterStr.equals("嘉義市流浪動物收容所")) {
            showUserAnimalShelterAddressStr = "嘉義市彌陀路101號";
        } else if (animalShelterStr.equals("基隆市寵物銀行")) {
            showUserAnimalShelterAddressStr = "基隆市七堵區大華三路45-12號";
        } else {
            showUserAnimalShelterAddressStr = animalShelterAddressStr;
        }
        return showUserAnimalShelterAddressStr;
    }
}
