package yvonneh.com.straysgohome;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by User on 2017/3/14.
 */

public class BitmapCacheManager implements ImageLoader.ImageCache {
    private LruCache<String, Bitmap> bitmapCacheManager;

    // 類別建構子
    public BitmapCacheManager() {

        // 獲取應用當前最大可使用記憶體，並為圖片快取分配額度
        int applicationMaxMemory = (int) Runtime.getRuntime().maxMemory();
        int bitmapCacheMemory = applicationMaxMemory / 8;

        bitmapCacheManager = new LruCache<String, Bitmap>(bitmapCacheMemory) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount();
            }
        };
    }

    @Override
    public Bitmap getBitmap(String imageURL) {
        return bitmapCacheManager.get(imageURL);
    }

    @Override
    public void putBitmap(String imageURL, Bitmap imageBitmap) {
        synchronized (bitmapCacheManager) {
            if (bitmapCacheManager.get(imageURL) == null) {
                bitmapCacheManager.put(imageURL, compressBitmapQuality(imageBitmap));
            }
        }
    }

    // 壓縮圖片品質＆縮圖的方法
    public static Bitmap compressBitmapQuality(Bitmap bitmap){

        // 壓縮圖片品質
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        int beginRate = 100;
        // bitmap.compress(圖片格式, 壓縮質量, 圖片資料流);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bOut);
        //如果壓縮後圖片大於100K，再度壓縮
        while(bOut.size()/1024/1024>100){
            beginRate -=10;
            bOut.reset();
            bitmap.compress(Bitmap.CompressFormat.JPEG, beginRate, bOut);
        }
        ByteArrayInputStream bInt = new ByteArrayInputStream(bOut.toByteArray());

        // 將圖片縮圖
        Bitmap newBitmap = BitmapFactory.decodeStream(bInt);
        int w = newBitmap.getWidth();
        int h = newBitmap.getHeight();
        int finalWidth = 480;
        int finalHeight = 800;
        float scaleWidth = ((float) finalWidth) / w;
        float scaleHeight = ((float) finalHeight) / h;
        Matrix martrix = new Matrix();
        martrix.postScale(scaleWidth, scaleHeight);
        Bitmap finalBitmap = Bitmap.createBitmap(newBitmap,0,0,w,h,martrix,true);

        // 回傳圖片
        if(finalBitmap!=null){
            return finalBitmap;
        }else{
            return bitmap;
        }
    }
}
