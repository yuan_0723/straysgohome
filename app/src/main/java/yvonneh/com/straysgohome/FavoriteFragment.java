package yvonneh.com.straysgohome;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static yvonneh.com.straysgohome.DataManager.DEFAULT_FAVORITE_STRAY_IDS;
import static yvonneh.com.straysgohome.DataManager.FAVORITE_STRAY_IDS_PREFERENCE_KEY;
import static yvonneh.com.straysgohome.DataManager.SHARED_PREFERENCE_NAME;

/**
 * Created by User on 2017/3/14.
 */

public class FavoriteFragment extends Fragment {
    // SharedPreferences物件
    static SharedPreferences sharedPreferences;
    static HashSet<String> favoriteStrayIDs;
    private static List<Stray> favoriteStrayList;
    private static DataManager dataManager;
    private static FavoriteAdapter favoriteAdapter;
    private static RecyclerView favoriteRecyclerView;

    // 建構子
    public FavoriteFragment() {
        dataManager = DataManager.shareInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // 載入收藏的浪浪編號
        sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFERENCE_NAME,MODE_PRIVATE);
        loadFavoriteStrayIDs();

        // 篩選已收藏的浪浪清單
        getFavoriteStrayList();

        // 將程式碼物件與Layout做關聯
        View favoriteView = inflater.inflate(R.layout.fragment_favorite, container, false);
        favoriteRecyclerView = (RecyclerView) favoriteView.findViewById(R.id.FavoriteRecyclerView);
        // 啟用客製ActionBar
        setHasOptionsMenu(true);

        // 設定最愛浪浪的RecyclerView
        favoriteRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        favoriteAdapter = new FavoriteAdapter(inflater);
        // 設定RecyclerView的轉接頭
        favoriteRecyclerView.setAdapter(favoriteAdapter);

        return favoriteView;
    }

    @Override
    public void onResume() {
        super.onResume();

        // 篩選已收藏的浪浪清單
        loadFavoriteStrayIDs();
        getFavoriteStrayList();
        favoriteAdapter.notifyDataSetChanged();

        // 檢查是否有收藏，若沒有則顯示教學圖
        setFavoriteBackground();
    }

    // 設定ActionBar對應的Layout檔
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.favorite_menu, menu);
    }
    // 設定ActionBar的點擊動作
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                // 檢查是否有收藏的浪浪編號
                if (favoriteStrayIDs.size() == 0) {
                    DeleteFavoriteAlertNone deleteFavoriteAlertNone = new DeleteFavoriteAlertNone();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    deleteFavoriteAlertNone.show(fragmentManager,"deleteFavoriteAlertNone");
                } else {
                    DeleteFavoriteAlert deleteFavoriteAlert = new DeleteFavoriteAlert();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    deleteFavoriteAlert.show(fragmentManager, "deleteFavoriteAlert");
                }
                break;
            default:
                return  super.onOptionsItemSelected(item);
        }
        return true;
    }

    // 讀取偏好設定的方法
    private static void loadFavoriteStrayIDs() {
        favoriteStrayIDs = (HashSet<String>) sharedPreferences.getStringSet(FAVORITE_STRAY_IDS_PREFERENCE_KEY, DEFAULT_FAVORITE_STRAY_IDS);
    }

    // 將收藏的浪浪編號集合寫入偏好設定的方法(務必要加上.clear()，否則在重啟後會遺失數據)
    private static void saveFavoriteStrayIDs(HashSet<String> favoriteStrayIDs) {
        sharedPreferences.edit()
                .clear()
                .putStringSet(FAVORITE_STRAY_IDS_PREFERENCE_KEY,favoriteStrayIDs)
                .apply();
    }

    // 篩選已收藏的浪浪清單
    private static void getFavoriteStrayList() {
        favoriteStrayList = new ArrayList<>();
        List<Stray> allStrayList = new ArrayList<>();
        allStrayList = dataManager.getAllStraysList();
        for (Iterator it = allStrayList.iterator(); it.hasNext();) {
            Stray tmpStray = (Stray)it.next();
            if (favoriteStrayIDs.contains(tmpStray.getAnimalID())) {
                favoriteStrayList.add(tmpStray);
            }
        }
    }

    private class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {

        private LayoutInflater favoriteInflater;
        private RequestQueue imageLoadQueue;
        private ImageLoader volleyImageLoader;

        public FavoriteAdapter(LayoutInflater favoriteInflater) {
            this.favoriteInflater = favoriteInflater;
            imageLoadQueue = Volley.newRequestQueue(getActivity());
            volleyImageLoader = new ImageLoader(imageLoadQueue, new BitmapCacheManager());
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView strayGenderKindImage;
            NetworkImageView strayPhotoImage;
            ImageButton favoriteBtn, cancelFavoriteBtn;
            TextView strayShelterNameTextView, strayBodyTypeTextView, strayAgeTextView, straySterilizationTextView, strayBacterinTextView;

            public ViewHolder(View itemView) {
                super(itemView);
                strayGenderKindImage = (ImageView) itemView.findViewById(R.id.strayGenderKindImage);
                strayPhotoImage = (NetworkImageView) itemView.findViewById(R.id.strayPhotoImage);
                favoriteBtn = (ImageButton) itemView.findViewById(R.id.favoriteBtn);
                cancelFavoriteBtn = (ImageButton) itemView.findViewById(R.id.cancelFavoriteBtn);
                strayShelterNameTextView = (TextView) itemView.findViewById(R.id.strayShelterNameTextView);
                strayBodyTypeTextView = (TextView) itemView.findViewById(R.id.strayBodyTypeTextView);
                strayAgeTextView = (TextView) itemView.findViewById(R.id.strayAgeTextView);
                straySterilizationTextView = (TextView) itemView.findViewById(R.id.straySterilizationTextView);
                strayBacterinTextView = (TextView) itemView.findViewById(R.id.strayBacterinTextView);
            }

        }

        @Override
        public int getItemCount() {
            return favoriteStrayList.size();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = favoriteInflater.inflate(R.layout.cardview_favorite, parent, false);
            ViewHolder viewHolder = new ViewHolder(itemView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            // 取得該CELL的Stray物件
            final  Stray stray = favoriteStrayList.get(position);

            // 要顯示的字串處理
            holder.strayShelterNameTextView.setText(stray.getAnimalShelterName());
            holder.strayBodyTypeTextView.setText(dataManager.showUserAnimalBodyTypeStr(stray.getAnimalBodyType()));
            holder.strayAgeTextView.setText(dataManager.showUserAnimalAgeStr(stray.getAnimalAge()));
            holder.straySterilizationTextView.setText(dataManager.showUserAnimalSterilizationStr(stray.getAnimalSterilization()));
            holder.strayBacterinTextView.setText(dataManager.showUserAnimalBacterinStr(stray.getAnimalBacterin()));

            // 要顯示的性別種類標籤
            holder.strayGenderKindImage.setImageResource(GetStrayGenderKindImage(stray.getAnimalKind(),stray.getAnimalGender()));

            // 設定照片顯示
            if (stray.getAnimalPhoto().length() == 0) {
                int noPhotoImageID;
                if (stray.getAnimalKind().equals("貓")) {
                    noPhotoImageID = R.drawable.cat_no_image;
                } else {
                    noPhotoImageID = R.drawable.dog_no_image;
                }
                holder.strayPhotoImage.setImageResource(noPhotoImageID);
            } else {
                holder.strayPhotoImage.setImageUrl(stray.getAnimalPhoto(), volleyImageLoader);
            }

            // 判斷是否顯示隱藏收藏按鈕
            // 判斷流浪動物編號是否在USERDEFAULT收藏中，若有則顯示取消收藏按鈕
//            if (favoriteStrayIDs.contains(stray.getAnimalID())) {
//                holder.cancelFavoriteBtn.setVisibility(ImageButton.VISIBLE);
//                holder.favoriteBtn.setVisibility(ImageButton.GONE);
//            } else {
//                holder.cancelFavoriteBtn.setVisibility(ImageButton.GONE);
//                holder.favoriteBtn.setVisibility(ImageButton.VISIBLE);
//            }

            holder.cancelFavoriteBtn.setVisibility(ImageButton.VISIBLE);
            holder.favoriteBtn.setVisibility(ImageButton.GONE);


//            // 設置按鈕點擊觸發事件
//            holder.favoriteBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    // 加入流浪動物編號到USERDEFAULT收藏
//                    favoriteStrayIDs.add(stray.getAnimalID());
//                    saveFavoriteStrayIDs(favoriteStrayIDs);
//
//                    // !!!隱藏/顯示對應按鈕
//                    v.setVisibility(ImageButton.GONE);
//                    loadFavoriteStrayIDs();
//                    getFavoriteStrayList();
//                    favoriteAdapter.notifyDataSetChanged();
//                    setFavoriteBackground();
////                    cancelFavoriteBtn.this.setVisibility(ImageButton.VISIBLE);
//                }
//            });

            holder.cancelFavoriteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 移除USERDEFAULT收藏的流浪動物編號
                    favoriteStrayIDs.remove(stray.getAnimalID());
                    saveFavoriteStrayIDs(favoriteStrayIDs);
                    // !!!隱藏/顯示對應按鈕
                    v.setVisibility(ImageButton.GONE);
                    loadFavoriteStrayIDs();
                    getFavoriteStrayList();
                    favoriteAdapter.notifyDataSetChanged();
                    setFavoriteBackground();
                }
            });

            // 設置點擊觸發跳轉事件
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), StrayInfoActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("strayInfo", stray);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        }

        // 用來判斷要顯示的性別種類標籤的方法
        private int GetStrayGenderKindImage(String kindStr, String genderStr) {
            int genderKindImageResID;
            if (kindStr.equals("貓")) {
                if (genderStr.equals("F")) {
                    genderKindImageResID = R.drawable.kind_catgirl;
                } else if (genderStr.equals("M")) {
                    genderKindImageResID = R.drawable.kind_catboy;
                } else {
                    genderKindImageResID = R.drawable.kind_catunknown;
                }
            } else {
                if (genderStr.equals("F")) {
                    genderKindImageResID = R.drawable.kind_doggirl;
                } else if (genderStr.equals("M")) {
                    genderKindImageResID = R.drawable.kind_dogboy;
                } else {
                    genderKindImageResID = R.drawable.kind_dogunknown;
                }
            }
            return genderKindImageResID;
        }
    }

    // 自訂刪除收藏的警告視窗類別，並實作點擊介面
    public static class DeleteFavoriteAlert extends DialogFragment implements DialogInterface.OnClickListener {

        public DeleteFavoriteAlert() {
        }

        @Override
        // 建立警告視窗元件
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog deleteFavoriteAlert = new AlertDialog.Builder(getActivity())
                    .setIcon(R.drawable.dialog_icon)
                    .setTitle("注意！")
                    .setMessage("確定要將最愛的浪浪資料全部刪除嗎？或是您可以選擇點擊愛心圖示個別刪除最愛的浪浪。")
                    .setPositiveButton("全部刪除", this)
                    .setNegativeButton("取消", this)
                    .create();
            return deleteFavoriteAlert;
        }

        // 警告視窗的點擊事件
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    favoriteStrayIDs.clear();
                    saveFavoriteStrayIDs(favoriteStrayIDs);
                    loadFavoriteStrayIDs();
                    getFavoriteStrayList();
                    favoriteAdapter.notifyDataSetChanged();
                    favoriteRecyclerView.setBackgroundResource(R.drawable.no_favorite_image);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.cancel();
                    break;
                default:
                    break;
            }
        }
    }

    // 自訂刪除收藏的警告視窗類別，並實作點擊介面
    public static class DeleteFavoriteAlertNone extends DialogFragment implements DialogInterface.OnClickListener {

        public DeleteFavoriteAlertNone() {
        }

        @Override
        // 建立警告視窗元件
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog deleteFavoriteAlertNone = new AlertDialog.Builder(getActivity())
                    .setTitle("您沒有任何的收藏")
                    .setMessage("您目前沒有任何的浪浪收藏資料。")
                    .setNeutralButton("知道了", this)
                    .create();
            return deleteFavoriteAlertNone;
        }

        // 警告視窗的點擊事件
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_NEUTRAL:
                    dialog.cancel();
                    break;
                default:
                    break;
            }
        }
    }

    // 設置背景圖的呈現的方法
    private void setFavoriteBackground() {
        if (favoriteStrayIDs.size() == 0) {
            Runnable backGroundRunnable = new Runnable() {
                @Override
                public void run() {
                    favoriteRecyclerView.setBackgroundResource(R.drawable.no_favorite_image);
                }
            };
            getActivity().runOnUiThread(backGroundRunnable);
        } else {
            Runnable cacelBackGroundRunnable = new Runnable() {
                @Override
                public void run() {
                    favoriteRecyclerView.setBackgroundResource(0);
                }
            };
            getActivity().runOnUiThread(cacelBackGroundRunnable);
        }
    }
}
