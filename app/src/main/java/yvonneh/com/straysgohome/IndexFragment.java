package yvonneh.com.straysgohome;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static yvonneh.com.straysgohome.DataManager.DEFAULT_INDEX_SHOW;
import static yvonneh.com.straysgohome.DataManager.INDEX_SHOW_SHARED_PREFERENCE_KEY;
import static yvonneh.com.straysgohome.DataManager.SHARED_PREFERENCE_NAME;

/**
 * Created by User on 2017/3/14.
 */

public class IndexFragment extends Fragment {
    private List<Stray> indexStraysList;
    private DataManager dataManager;
    private int indexShow;

    // 建構子
    public IndexFragment() {
        dataManager = DataManager.shareInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // 載入首頁顯示設定
        loadSharedPreferences();
//        indexShow = 2;

        // 根據首頁顯示設定篩選要呈現在首頁上浪浪類型的方法
        indexStraysList = showStraysBySetting(indexShow);

        // 將程式碼物件與Layout檔做關聯
        View indexView = inflater.inflate(R.layout.fragment_index, container, false);
        RecyclerView indexRecyclerView = (RecyclerView) indexView.findViewById(R.id.indexRecyclerView);
        // 啟用客製的ACTIONBAR
        setHasOptionsMenu(true);

        // 設定首頁的RecyclerView
        indexRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        // 設定RecyclerView轉接頭
        indexRecyclerView.setAdapter(new IndexAdapter(inflater));
        return indexView;
    }

    // 設定ActionBar對應的MenuLayout檔
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.index_menu, menu);
    }

    // 設定ActionBar的點擊動作
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                // 跳轉至搜尋頁面
//                Toast.makeText(getActivity(),"搜尋被按了",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), StraySearchActivity.class);
                startActivity(intent);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private class IndexAdapter extends RecyclerView.Adapter<IndexAdapter.ViewHolder> {

        private LayoutInflater indexInflater;

        // VOLLEY圖片快取相關物件
        private RequestQueue imageLoadQueue;
        private ImageLoader volleyImageLoader;

        // 建構子
        public IndexAdapter (LayoutInflater indexInflater) {
            this.indexInflater = indexInflater;
            imageLoadQueue = Volley.newRequestQueue(getActivity());
            volleyImageLoader = new ImageLoader(imageLoadQueue, new BitmapCacheManager());
        }

        // 設定首頁CARDVIEW的HOLDER
        class ViewHolder extends RecyclerView.ViewHolder {
            TextView strayLocationTextView;
            ImageView strayGenderKindImage;
            NetworkImageView strayPhotoImage;

            public ViewHolder(View itemView) {
                super(itemView);
                strayLocationTextView = (TextView) itemView.findViewById(R.id.strayLocationTextView);
                strayGenderKindImage = (ImageView) itemView.findViewById(R.id.strayGenderKindImage);
                strayPhotoImage = (NetworkImageView) itemView.findViewById(R.id.strayPhotoImage);
            }
        }

        @Override
        public int getItemCount() {
            return indexStraysList.size();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = indexInflater.inflate(R.layout.cardview_index, parent, false);
            ViewHolder viewHolder = new ViewHolder(itemView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            // 取得當下ITEM的物件
            final Stray stray = indexStraysList.get(position);

            // 根據收容所名稱設定所在地字串（需校正苗栗縣）
            String strayLocationStr = stray.getAnimalShelterName().substring(0, 3);
            if (strayLocationStr.equals("苗栗動")) {
                strayLocationStr = "苗栗縣";
            }
            viewHolder.strayLocationTextView.setText(strayLocationStr);

            // 設定要顯示的性別種類標籤
            viewHolder.strayGenderKindImage.setImageResource
                    (GetStrayGenderKindImage(stray.getAnimalKind(), stray.getAnimalGender()));

            // 設定照片顯示
            if (stray.getAnimalPhoto().length() == 0) {
                int noPhotoImageID;
                if (stray.getAnimalKind().equals("貓")) {
                    noPhotoImageID = R.drawable.cat_no_image;
                } else {
                    noPhotoImageID = R.drawable.dog_no_image;
                }
                viewHolder.strayPhotoImage.setImageResource(noPhotoImageID);
            } else {
                // 如果有照片網址才執行下載
                viewHolder.strayPhotoImage.setImageUrl(stray.getAnimalPhoto(), volleyImageLoader);
            }

            // 設置點擊觸發跳轉事件
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), StrayInfoActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("strayInfo",stray);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        }

        // 用來判斷要顯示的性別種類標籤的方法
        private int GetStrayGenderKindImage(String kindStr, String genderStr) {
            int genderKindImageResID;
            if (kindStr.equals("貓")) {
                if (genderStr.equals("F")) {
                    genderKindImageResID = R.drawable.kind_catgirl;
                } else if (genderStr.equals("M")) {
                    genderKindImageResID = R.drawable.kind_catboy;
                } else {
                    genderKindImageResID = R.drawable.kind_catunknown;
                }
            } else {
                if (genderStr.equals("F")) {
                    genderKindImageResID = R.drawable.kind_doggirl;
                } else if (genderStr.equals("M")) {
                    genderKindImageResID = R.drawable.kind_dogboy;
                } else {
                    genderKindImageResID = R.drawable.kind_dogunknown;
                }
            }
            return genderKindImageResID;
        }
    }
    // 讀取偏好設定的方法
    private void loadSharedPreferences() {
        // getSharedPreferences(偏好設定檔名稱, 存取權限)，創造一個SharedPreferences物件
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFERENCE_NAME,MODE_PRIVATE);
        indexShow = sharedPreferences.getInt(INDEX_SHOW_SHARED_PREFERENCE_KEY, DEFAULT_INDEX_SHOW);
    }

    // 根據首頁顯示設定篩選要呈現在首頁上浪浪類型的方法
    private ArrayList<Stray> showStraysBySetting(int indexshow) {
        List<Stray> allStraysList = new ArrayList<>();
        ArrayList<Stray> tmpIndexShow = new ArrayList<>();
        allStraysList = dataManager.getAllStraysList();
        if (indexshow == 1) {
            for (Iterator it = allStraysList.iterator();it.hasNext();) {
                Stray tmpStray = (Stray) it.next();
                if (tmpStray.getAnimalKind().equals("貓")){
                    tmpIndexShow.add(tmpStray);
                }
            }
        } else if (indexshow == 2) {
            for (Iterator it = allStraysList.iterator();it.hasNext();) {
                Stray tmpStray = (Stray) it.next();
                if (tmpStray.getAnimalKind().equals("狗")){
                    tmpIndexShow.add(tmpStray);
                }
            }
        } else {
            for (Iterator it = allStraysList.iterator();it.hasNext();) {
                Stray tmpStray = (Stray) it.next();
                tmpIndexShow.add(tmpStray);
            }
        }
        return tmpIndexShow;
    }
}
