package yvonneh.com.straysgohome;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static yvonneh.com.straysgohome.DataManager.STRAYS_JSON_URL;

public class MainActivity extends AppCompatActivity {

    // 下載資料進度條
    private ProgressDialog downloadProgressDialog;
    // 存放流浪動物資料的List集合
    private List <Stray> downloadStraysList;

    // 頁面List集合用以存放PAGER會使用到的頁面
    private List<Page> pagerContentList;

    TabLayout initialTabLayout;
    ViewPager initialViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 初始化要存放在PAGER中的頁面
        initialPagerContentList();

        // 將TOOLBAR與LAYOUT檔建立關聯
        Toolbar initialToolbar = (Toolbar) findViewById(R.id.initialToolbar);
        initialToolbar.setLogo(R.mipmap.toolbar_icon);
        // 設定Toolbar上的ActionBar
        setSupportActionBar(initialToolbar);
        // 初始化第一個頁面的Toolbar標題
        getSupportActionBar().setTitle(pagerContentList.get(0).getPageTitle());

        // 將TabLayout與Layout檔建立關聯
        initialTabLayout = (TabLayout) findViewById(R.id.initialTabLayout);
        // 將ViewPager與Layout檔建立關聯
        initialViewPager = (ViewPager) findViewById(R.id.initialViewPager);
        // 將ViewPager物件與TabLayout物件做關聯
        initialTabLayout.setupWithViewPager(initialViewPager);

        // 判斷網路狀態，若有連線才執行下載
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // 執行非同步下載JSON資料
            new GetJsonData().execute(STRAYS_JSON_URL);
        } else {
            // 顯示警告視窗
            NoInternetAlert noInternetAlert = new NoInternetAlert();
            FragmentManager fragmentManager = getSupportFragmentManager();
            noInternetAlert.show(fragmentManager,"NoInternetAlert");
            initialViewPager.setBackgroundResource(R.drawable.no_internet_image);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    // 初始化PAGER對應的FRAGMENT頁面
    private void initialPagerContentList(){
        pagerContentList = new ArrayList<>();
        pagerContentList.add(new Page("浪浪回家",new IndexFragment()));
        pagerContentList.add(new Page("最愛浪浪",new FavoriteFragment()));
        pagerContentList.add(new Page("進階設定",new SettingFragment()));
    }

    // 將Pager接上轉接頭的方法
    private void PagerAdapterSetting(){

        // 設定ViewPager的轉接頭（下載完資料後設定）
        initialViewPager.setAdapter(new InitialPagerAdapter(getSupportFragmentManager()));
        initialViewPager.setOffscreenPageLimit(2);
        // 覆寫ViewPager的監聽事件
        initialViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                //當頁面被選擇時，同時改變Toolbar標題
                getSupportActionBar().setTitle(pagerContentList.get(position).getPageTitle());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    // 自訂Pager的轉接頭類別
    private class InitialPagerAdapter extends FragmentPagerAdapter {

        // 建構子
        public InitialPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Page要呈現的Fragment資料
        @Override
        public Fragment getItem(int position) {
            return pagerContentList.get(position).getContentFragment();
        }

        // Pager內共有幾個Page
        @Override
        public int getCount() {
            return pagerContentList.size();
        }

        // 每個Page對應的標題
        @Override
        public CharSequence getPageTitle(int position) {
            return pagerContentList.get(position).getPageTitle();
        }
    }

    // 非同步下載JSON資料的內部類別
    private class GetJsonData extends AsyncTask<String, Integer, List<Stray>> {

        @Override
        // 下載資料前的準備工作
        protected void onPreExecute() {
            // 顯示資料下載進度條
            downloadProgressDialog = new ProgressDialog(MainActivity.this);
            downloadProgressDialog.setMessage("資料下載中，請稍後...");
            downloadProgressDialog.show();
        }

        @Override
        // 在背景執行緒要執行的事情
        protected List<Stray> doInBackground(String... taskParams) {
            String firstTaskURL = taskParams[0];
            // 準備用以承接連線資料的可變動字串容器
            StringBuilder allConnectionStr = new StringBuilder();

            try {

                // 建立連線物件，並設定其細項參數
                URL taskURL = new URL(firstTaskURL);
                HttpURLConnection urlConnection = (HttpURLConnection)taskURL.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                // 執行連線
                urlConnection.connect();

                // 根據連線回應觸發對應的事件
                int connectionResponse = urlConnection.getResponseCode();
                if (connectionResponse == HttpURLConnection.HTTP_OK) {
                    String connectionStr;

                    // 若連線回應正確，處理資料流
                    InputStream connectionDataIS = urlConnection.getInputStream();
                    InputStreamReader connectionDataISR = new InputStreamReader(connectionDataIS);
                    BufferedReader connectionDataBR = new BufferedReader(connectionDataISR);

                    // 利用迴圈將所有連線取得的資料串接在一起
                    while ((connectionStr = connectionDataBR.readLine()) != null) {
                        allConnectionStr.append(connectionStr);
                    }

                    // 關閉資料流處理通道（後開先關）
                    connectionDataBR.close();
                    connectionDataISR.close();
                    connectionDataIS.close();
                }

            } catch (MalformedURLException e) {
                // 例外處理
                Log.e("MainActivity", e.toString());
            } catch (IOException e) {
                Log.e("MainActivity", e.toString());
            }

            if (allConnectionStr.length() > 0) {
                try {
                    // 若allConnectionStr有字串資料則從中獲取List物件
                    downloadStraysList = getStraysList(allConnectionStr.toString());
                    return downloadStraysList;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        // 執行背景下載完成後要執行的事情
        protected void onPostExecute(List<Stray> strays) {

            // 將處理完的流浪動物集合寫入至SINGLETON中
            DataManager dataManager = DataManager.shareInstance();
            dataManager.setAllStraysList(downloadStraysList);

            // 街上 Pager 轉街頭
            PagerAdapterSetting();

            // 載入資料旋轉圖示停止
            downloadProgressDialog.dismiss();
        }
    }

    // 將連線取得的JSON字串資料解析為List物件的方法
    private List<Stray> getStraysList(String allConnectionStr) throws JSONException {

        List<Stray> tmpStraysList = new ArrayList<>();

        // 根據原始Json內容解析Json字串（{}:getJsonObject / []:getJsonArray）
        JSONArray jsonDataArray = new JSONArray(allConnectionStr);

        // 利用迴圈處理JSON陣列下的物件資料，並成立新物件塞入LIST內
        for (int i=0; i<jsonDataArray.length(); i++) {

            JSONObject strayInfo = jsonDataArray.getJSONObject(i);

            String animalID = strayInfo.getString("animal_id");
            String animalPhoto = strayInfo.getString("album_file");
            String animalKind = strayInfo.getString("animal_kind");
            String animalGender = strayInfo.getString("animal_sex");
            String animalBodyType = strayInfo.getString("animal_bodytype");
            String animalColour = strayInfo.getString("animal_colour");
            String animalAge = strayInfo.getString("animal_age");
            String animalSterilization = strayInfo.getString("animal_sterilization");
            String animalBacterin = strayInfo.getString("animal_bacterin");
            String animalRemark = strayInfo.getString("animal_remark");
            String animalCaption = strayInfo.getString("animal_caption");
            String animalOpenDate = strayInfo.getString("animal_opendate");
            String animalUpdate = strayInfo.getString("animal_update");
            String animalCreateTime = strayInfo.getString("animal_createtime");
            String animalShelterName = strayInfo.getString("shelter_name");
            String animalShelterAddress = strayInfo.getString("shelter_address");
            String animalShelterTEL = strayInfo.getString("shelter_tel");

            Stray tmpStray = new Stray(animalID, animalPhoto, animalKind, animalGender, animalBodyType, animalColour, animalAge, animalSterilization, animalBacterin, animalRemark, animalCaption, animalOpenDate, animalUpdate, animalCreateTime, animalShelterName, animalShelterAddress, animalShelterTEL);
            tmpStraysList.add(tmpStray);
        }
        return tmpStraysList;
    }

    // 自訂沒網路時的警告視窗類別，並實作點擊介面
    public static class NoInternetAlert extends DialogFragment implements DialogInterface.OnClickListener {

        public NoInternetAlert() {}

        @Override
        // 建立警告視窗元件
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog noInternetAlert = new AlertDialog.Builder(getActivity())
                    .setIcon(R.drawable.dialog_icon)
                    .setTitle("無法連結網路")
                    .setMessage("請前往設定開啟您的網路後並重新開啟APP連接政府資料庫")
                    .setNeutralButton("知道了", this)
                    .create();
            return  noInternetAlert;
        }

        // 警告視窗的點擊事件
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_NEUTRAL:
                    dialog.cancel();
                default:
                    break;
            }
        }
    }
}
