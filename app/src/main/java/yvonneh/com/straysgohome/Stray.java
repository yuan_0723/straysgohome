package yvonneh.com.straysgohome;

import java.io.Serializable;

/**
 * Created by User on 2017/3/14.
 */

public class Stray implements Serializable {
    // 宣告類別下的屬性
    private String animalID;
    private String animalPhoto;
    private String animalKind;
    private String animalGender;
    private String animalBodyType;
    private String animalColour;
    private String animalAge;
    private String animalSterilization;
    private String animalBacterin;
    private String animalRemark;
    private String animalCaption;
    private String animalOpenDate;
    private String animalUpdate;
    private String animalCreateTime;
    private String animalShelterName;
    private String animalShelterAddress;
    private String animalShelterTEL;

    // 建立建構子
    public Stray() {
    }

    public Stray(String animalID, String animalPhoto, String animalKind, String animalGender, String animalBodyType, String animalColour, String animalAge, String animalSterilization, String animalBacterin, String animalRemark, String animalCaption, String animalOpenDate, String animalUpdate, String animalCreateTime, String animalShelterName, String animalShelterAddress, String animalShelterTEL) {
        this.animalID = animalID;
        this.animalPhoto = animalPhoto;
        this.animalKind = animalKind;
        this.animalGender = animalGender;
        this.animalBodyType = animalBodyType;
        this.animalColour = animalColour;
        this.animalAge = animalAge;
        this.animalSterilization = animalSterilization;
        this.animalBacterin = animalBacterin;
        this.animalRemark = animalRemark;
        this.animalCaption = animalCaption;
        this.animalOpenDate = animalOpenDate;
        this.animalUpdate = animalUpdate;
        this.animalCreateTime = animalCreateTime;
        this.animalShelterName = animalShelterName;
        this.animalShelterAddress = animalShelterAddress;
        this.animalShelterTEL = animalShelterTEL;
    }

    //Property 的 Getter / Setter

    public String getAnimalShelterTEL() {
        return animalShelterTEL;
    }

    public void setAnimalShelterTEL(String animalShelterTEL) {
        this.animalShelterTEL = animalShelterTEL;
    }

    public String getAnimalID() {
        return animalID;
    }

    public void setAnimalID(String animalID) {
        this.animalID = animalID;
    }

    public String getAnimalPhoto() {
        return animalPhoto;
    }

    public void setAnimalPhoto(String animalPhoto) {
        this.animalPhoto = animalPhoto;
    }

    public String getAnimalKind() {
        return animalKind;
    }

    public void setAnimalKind(String animalKind) {
        this.animalKind = animalKind;
    }

    public String getAnimalGender() {
        return animalGender;
    }

    public void setAnimalGender(String animalGender) {
        this.animalGender = animalGender;
    }

    public String getAnimalBodyType() {
        return animalBodyType;
    }

    public void setAnimalBodyType(String animalBodyType) {
        this.animalBodyType = animalBodyType;
    }

    public String getAnimalColour() {
        return animalColour;
    }

    public void setAnimalColour(String animalColour) {
        this.animalColour = animalColour;
    }

    public String getAnimalAge() {
        return animalAge;
    }

    public void setAnimalAge(String animalAge) {
        this.animalAge = animalAge;
    }

    public String getAnimalSterilization() {
        return animalSterilization;
    }

    public void setAnimalSterilization(String animalSterilization) {
        this.animalSterilization = animalSterilization;
    }

    public String getAnimalBacterin() {
        return animalBacterin;
    }

    public void setAnimalBacterin(String animalBacterin) {
        this.animalBacterin = animalBacterin;
    }

    public String getAnimalRemark() {
        return animalRemark;
    }

    public void setAnimalRemark(String animalRemark) {
        this.animalRemark = animalRemark;
    }

    public String getAnimalCaption() {
        return animalCaption;
    }

    public void setAnimalCaption(String animalCaption) {
        this.animalCaption = animalCaption;
    }

    public String getAnimalOpenDate() {
        return animalOpenDate;
    }

    public void setAnimalOpenDate(String animalOpenDate) {
        this.animalOpenDate = animalOpenDate;
    }

    public String getAnimalUpdate() {
        return animalUpdate;
    }

    public void setAnimalUpdate(String animalUpdate) {
        this.animalUpdate = animalUpdate;
    }

    public String getAnimalCreateTime() {
        return animalCreateTime;
    }

    public void setAnimalCreateTime(String animalCreateTime) {
        this.animalCreateTime = animalCreateTime;
    }

    public String getAnimalShelterName() {
        return animalShelterName;
    }

    public void setAnimalShelterName(String animalShelterName) {
        this.animalShelterName = animalShelterName;
    }

    public String getAnimalShelterAddress() {
        return animalShelterAddress;
    }

    public void setAnimalShelterAddress(String animalShelterAddress) {
        this.animalShelterAddress = animalShelterAddress;
    }

    private static final long serialVersionUID = 1L;
}
