package yvonneh.com.straysgohome;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static yvonneh.com.straysgohome.DataManager.DEFAULT_FAVORITE_STRAY_IDS;
import static yvonneh.com.straysgohome.DataManager.FAVORITE_STRAY_IDS_PREFERENCE_KEY;
import static yvonneh.com.straysgohome.DataManager.PERMISSION_REQUEST_CODE;
import static yvonneh.com.straysgohome.DataManager.SHARED_PREFERENCE_NAME;

/**
 * Created by User on 2017/3/15.
 */

public class StrayInfoActivity extends AppCompatActivity implements OnMapReadyCallback {

    // 流浪動物資料物件
    Stray stray;
    // 單例物件
    DataManager dataManager;

    // VOLLEY圖片快取相關物件
    private RequestQueue imageLoadQueue;
    private ImageLoader volleyImageLoader;
    NetworkImageView strayPhotoImage;

    // SharedPreferences物件
    SharedPreferences sharedPreferences;
    // 收藏的浪浪編號集合
    HashSet<String> favoriteStrayIDs;

    // 地圖相關物件
    private GoogleMap shelterGoogleMap;
    SupportMapFragment mapViewFragment;

    // 會使用到的基本UI相關物件
    TextView strayIDTextView, strayKindTextView, strayGenderTextView, strayBodyTypeTextView,
             strayClourTextView, strayAgeTextView, straySterilizationTextView, strayBacterinTextView,
             strayNoteTextView, strayShelterAddressTextView, strayOpenDateTextView, strayUpdateTextView;
    RelativeLayout strayNoteRelativeLayout, shelterNameRelativeLayout, shelterPhoneRelativeLayout,
                    shelterAddressRelativeLayout, shelterMapRelativeLayout;
    Toolbar strayInfoToolbar;
    ImageButton favoriteButton, cancelFavoriteButton;
    static TextView strayShelterNameTextView, strayShelterPhoneTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stray_info);

        // 請求定位權限
        askPermissions();

        // 初始化圖片快取物件
        imageLoadQueue = Volley.newRequestQueue(StrayInfoActivity.this);
        volleyImageLoader = new ImageLoader(imageLoadQueue, new BitmapCacheManager());

        // 取得上一頁所傳來的INTENT內BUNDLE資料
        stray = (Stray) getIntent().getExtras().getSerializable("strayInfo");

        // 產生單例物件
        dataManager = DataManager.shareInstance();

        sharedPreferences = getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        // 載入偏好設定
        loadFavoriteStrayIDs();

        // 建立Layout檔與程式碼的物件關聯
        findViews();

        // 判斷要顯示 收藏 / 取消收藏 按鈕
        showOrHiddenFavoriteButton();

        // 判斷該列是否無資料，若無資料則隱藏
        hiddenNoDataRelativeLayout();

        // 呈現浪浪大頭照
        presentStrayPhoto();

        // 呈現字串資訊
        presentStrayInfoStr();

        // 設置監聽事件
        setupOnClickListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.stray_info_menu, menu);
        return true;
    }

    @Override
    // fragment 載入地圖成功後系統自動呼叫
    public void onMapReady(GoogleMap googleMap) {
        this.shelterGoogleMap = googleMap;
        // 對地圖做相關設定
        settingMap();
        // 根據收容所地址將MARK呈現在地圖上
        turnShelterAddressToMarker((String)strayShelterAddressTextView.getText());
    }

    @Override
    // ACTIONBAR MENU 觸發事件覆寫
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_share:
                shareStrayInfo();
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    // 建立Layout檔與程式碼的物件關聯的方法
    private void findViews() {

        // 設定Toolbar
        setupToolbar();

        // 載入MapFragment
        setupMapFragment();

        // 關聯LAYOUT檔圖片物件
        strayPhotoImage = (NetworkImageView) findViewById(R.id.strayPhotoImage);

        // 關聯LAYOUT檔圖片按鈕物件
        favoriteButton = (ImageButton) findViewById(R.id.favoriteBtn);
        cancelFavoriteButton = (ImageButton) findViewById(R.id.cancelFavoriteBtn);

        // 關聯LAYOUT檔RelativeLayout
        strayNoteRelativeLayout = (RelativeLayout) findViewById(R.id.strayNoteRelativeLayout);
        shelterNameRelativeLayout = (RelativeLayout) findViewById(R.id.shelterNameRelativeLayout);
        shelterPhoneRelativeLayout = (RelativeLayout) findViewById(R.id.shelterPhoneRelativeLayout);
        shelterAddressRelativeLayout = (RelativeLayout) findViewById(R.id.shelterAddressRelativeLayout);
        shelterMapRelativeLayout = (RelativeLayout) findViewById(R.id.shelterMapRelativeLayout);

        // 關聯LAYOUT檔文字物件
        strayIDTextView = (TextView) findViewById(R.id.strayIDTextView);
        strayKindTextView = (TextView) findViewById(R.id.strayKindTextView);
        strayGenderTextView = (TextView) findViewById(R.id.strayGenderTextView);
        strayBodyTypeTextView = (TextView) findViewById(R.id.strayBodyTypeTextView);
        strayClourTextView = (TextView) findViewById(R.id.strayClourTextView);
        strayAgeTextView = (TextView) findViewById(R.id.strayAgeTextView);
        straySterilizationTextView = (TextView) findViewById(R.id.straySterilizationTextView);
        strayBacterinTextView = (TextView) findViewById(R.id.strayBacterinTextView);
        strayNoteTextView = (TextView) findViewById(R.id.strayNoteTextView);
        strayShelterNameTextView = (TextView) findViewById(R.id.strayShelterNameTextView);
        strayShelterPhoneTextView = (TextView) findViewById(R.id.strayShelterPhoneTextView);
        strayShelterAddressTextView = (TextView) findViewById(R.id.strayShelterAddressTextView);
        strayOpenDateTextView = (TextView) findViewById(R.id.strayOpenDateTextView);
        strayUpdateTextView = (TextView) findViewById(R.id.strayUpdateTextView);
    }

    // 設定Toolbar的方法
    private void setupToolbar() {
        strayInfoToolbar = (Toolbar) findViewById(R.id.straySearchToolbar);
        strayInfoToolbar.setTitle("認養詳情");
        setSupportActionBar(strayInfoToolbar);

        // 顯示ACTIONBAR上的返回鍵
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // 載入MapFragment的方法
    private void setupMapFragment() {
        mapViewFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.shelterMapView);
        // 非同步加載地圖（加載完成後觸發onMapReady）
        mapViewFragment.getMapAsync(this);
    }

    // 設定Map的方法
    private void settingMap() {

        // 獲取用以設定地圖的物件
        UiSettings mapSettings = shelterGoogleMap.getUiSettings();
        // 是否顯示交通流量
        shelterGoogleMap.setTrafficEnabled(false);

        // 是否呈現「自己位置的圖層」
        try {
            shelterGoogleMap.setMyLocationEnabled(false);
        } catch (SecurityException e) {
            Log.e("SecurityException",e.toString());
        }

        // 是否呈現地圖縮放按鈕
        mapSettings.setZoomControlsEnabled(false);
        // 是否顯示指北針
        mapSettings.setCompassEnabled(false);
        // 是否呈現「找到自己位置的按鈕」
        mapSettings.setMyLocationButtonEnabled(false);
        // 始否開啟捲動手勢
        mapSettings.setScrollGesturesEnabled(false);
        // 是否開啟縮放手勢
        mapSettings.setZoomGesturesEnabled(false);
        // 是否開啟傾斜手勢
        mapSettings.setTiltGesturesEnabled(false);
        // 是否開啟旋轉手勢
        mapSettings.setRotateGesturesEnabled(false);
        shelterGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // 跳頁到大地圖
//                Toast.makeText(StrayInfoActivity.this,"跳頁到大地圖",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(StrayInfoActivity.this, ShelterMapActivity.class);
                intent.putExtra("shelterAddress", (String) strayShelterAddressTextView.getText());
                intent.putExtra("shelterName", (String) strayShelterNameTextView.getText());
                startActivity(intent);
            }
        });
    }

    // 將收容所地址轉為位置後再地圖上呈現Marker的方法
    private void turnShelterAddressToMarker(String shelterAddressStr) {

        // 增加新標記前，消除舊的Marker
        shelterGoogleMap.clear();
        Geocoder googleGeocoder = new Geocoder(this);
        // 解析地址後可能產生多筆位置資料，所以準備一個集合來承接接資訊;
        List<Address> geocoderAnalysisResultList = null;

        try {
            //Geocoder.getFromLocationName(地址字串, 需要回傳的筆數);
            geocoderAnalysisResultList = googleGeocoder.getFromLocationName(shelterAddressStr, 1);
        } catch (IOException ie) {
            Log.e("Geocoder",ie.toString());
        }

        if (geocoderAnalysisResultList == null || geocoderAnalysisResultList.isEmpty()) {
            // 隱藏地圖RELATIVELAYOUT
            shelterMapRelativeLayout.setVisibility(RelativeLayout.GONE);
        } else {

            // 取得地址分析後的第一筆位置資料
            Address shelterAddress = geocoderAnalysisResultList.get(0);
            // 從Address物件取出LatLng物件
            LatLng shelterLatLng = new LatLng(shelterAddress.getLatitude(), shelterAddress.getLongitude());

            String shelterMarkSnippet = (String) strayShelterAddressTextView.getText();

            // 將地址轉成位置後在地圖上打上對應的標記
            shelterGoogleMap.addMarker(new MarkerOptions().position(shelterLatLng)
                                                          .title(stray.getAnimalShelterName())
                                                          .snippet(shelterMarkSnippet));

            // 將地圖的鏡頭對焦在收容所的地址標記上
            CameraPosition mapCameraPosition = new CameraPosition.Builder().target(shelterLatLng)
                                                                           .zoom(16)
                                                                           .build();
//            shelterGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(mapCameraPosition));
            shelterGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(mapCameraPosition));
        }
    }

    // 將字串資訊呈現在畫面上的方法
    private void presentStrayInfoStr() {
        strayIDTextView.setText(stray.getAnimalID());
        strayClourTextView.setText(stray.getAnimalColour());
        strayShelterNameTextView.setText(stray.getAnimalShelterName());
        strayOpenDateTextView.setText(stray.getAnimalOpenDate());
        strayUpdateTextView.setText(stray.getAnimalUpdate().substring(0,10));
        strayAgeTextView.setText(dataManager.showUserAnimalAgeStr(stray.getAnimalAge()));
        strayKindTextView.setText(dataManager.showUserAnimalKindStr(stray.getAnimalKind()));
        strayGenderTextView.setText(dataManager.showUserAnimalGenderStr(stray.getAnimalGender()));
        strayBodyTypeTextView.setText(dataManager.showUserAnimalBodyTypeStr(stray.getAnimalBodyType()));
        strayBacterinTextView.setText(dataManager.showUserAnimalBacterinStr(stray.getAnimalBacterin()));
        straySterilizationTextView.setText(dataManager.showUserAnimalSterilizationStr(stray.getAnimalSterilization()));
        strayNoteTextView.setText(dataManager.showUserAnimalNoteStr(stray.getAnimalRemark(),stray.getAnimalCaption()));
        strayShelterPhoneTextView.setText(dataManager.showUserAnimalShelterPhoneStr(stray.getAnimalShelterName(), stray.getAnimalShelterTEL()));
        strayShelterAddressTextView.setText(dataManager.showUserAnimalShelterAddress(stray.getAnimalShelterName(),stray.getAnimalShelterAddress()));
    }

    // 呈現浪浪大頭照的方法
    private void presentStrayPhoto() {
        if (stray.getAnimalPhoto().length() == 0) {
            // 如果沒有照片網址顯示預設圖檔
            int noPhotoImageResID;
            if (stray.getAnimalKind().equals("貓")) {
                noPhotoImageResID = R.drawable.cat_no_image;
            } else {
                noPhotoImageResID = R.drawable.dog_no_image;
            }
            strayPhotoImage.setImageResource(noPhotoImageResID);
        } else {
            // 如果有照片網址才下載照片圖檔
            strayPhotoImage.setImageUrl(stray.getAnimalPhoto(), volleyImageLoader);
        }
    }

    // 判斷是否顯示隱藏收藏按鈕
    private void showOrHiddenFavoriteButton() {
        // 判斷流浪動物編號是否在USERDEFAULT收藏中，若有則顯示取消收藏按鈕
        if (favoriteStrayIDs.contains(stray.getAnimalID())) {
            cancelFavoriteButton.setVisibility(ImageButton.VISIBLE);
            favoriteButton.setVisibility(ImageButton.GONE);
        } else {
            cancelFavoriteButton.setVisibility(ImageButton.GONE);
            favoriteButton.setVisibility(ImageButton.VISIBLE);
        }
    }

    // 決定是否隱藏RELATIVELAYOUT的方法
    private void hiddenNoDataRelativeLayout() {
        // 判斷是否隱藏「其他備註」RelativeLayout
        if (stray.getAnimalRemark().equals("") && stray.getAnimalCaption().equals("")) {
            strayNoteRelativeLayout.setVisibility(View.GONE);
        }
        // 判斷是否隱藏「收容所電話」RelativeLayout
        if (stray.getAnimalShelterTEL().equals("")) {
            shelterPhoneRelativeLayout.setVisibility(View.GONE);
        }
        // 判斷是否隱藏「收容所住址」及「地圖」的RelativeLayout
        if (stray.getAnimalShelterAddress().equals("")) {
            shelterAddressRelativeLayout.setVisibility(View.GONE);
            shelterMapRelativeLayout.setVisibility(View.GONE);
        }
    }

    // 設置監聽事件的方法
    private void setupOnClickListeners() {

        // 點擊收容所RelativeLayout時觸發的方法
        shelterNameRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 跳頁到GOOGLE搜尋收容所
                String googleSearchStr = "http://www.google.com/search?q=" + stray.getAnimalShelterName();
                Uri googleSearchURI = Uri.parse(googleSearchStr);
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, googleSearchURI);
                startActivity(launchBrowser);
            }
        });

        // 點擊收容所電話RelativeLayout觸發的方法
        shelterPhoneRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 檢查是否有允許撥打電話的權限
                if (ContextCompat.checkSelfPermission(StrayInfoActivity.this, android.Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // 若沒有則申請撥打電話的權限
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE);

                } else {
                    // 若有權限則進行撥號
                    CallPhoneAlert callPhoneAlert = new CallPhoneAlert();
                    FragmentManager fragmentManager = StrayInfoActivity.this.getSupportFragmentManager();
                    callPhoneAlert.show(fragmentManager, "callShelterPhone");
                }
            }
        });

        // 點擊收容所地址RelativeLayout觸發的方法
        shelterAddressRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 跳頁到滿版地圖頁
            }
        });

        // 點擊收藏按鈕時觸發的方法
        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 加入流浪動物編號到USERDEFAULT收藏
                favoriteStrayIDs.add(stray.getAnimalID());
                saveFavoriteStrayIDs(favoriteStrayIDs);

                // 隱藏/顯示對應按鈕
                favoriteButton.setVisibility(ImageButton.GONE);
                cancelFavoriteButton.setVisibility(ImageButton.VISIBLE);
            }
        });

        // 點擊取消收藏按鈕時觸發的方法
        cancelFavoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 移除USERDEFAULT收藏的流浪動物編號
                favoriteStrayIDs.remove(stray.getAnimalID());
                saveFavoriteStrayIDs(favoriteStrayIDs);

                // 隱藏/顯示對應按鈕
                cancelFavoriteButton.setVisibility(ImageButton.GONE);
                favoriteButton.setVisibility(ImageButton.VISIBLE);
            }
        });
    }
    private void askPermissions() {
        String[] permissions = {
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION
        };

        Set<String> permissionsRequest = new HashSet<>();
        for (String permission : permissions) {
            int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                permissionsRequest.add(permission);
            }
        }

        if (!permissionsRequest.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    permissionsRequest.toArray(new String[permissionsRequest.size()]),
                    PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                String text = "";
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        text += permissions[i] + "\n";
                    }
                }
                if (!text.isEmpty()) {
                    text = "NO PERMISSION";
                    Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    // 讀取偏好設定的方法
    private void loadFavoriteStrayIDs() {
        favoriteStrayIDs = (HashSet<String>) sharedPreferences.getStringSet(FAVORITE_STRAY_IDS_PREFERENCE_KEY, DEFAULT_FAVORITE_STRAY_IDS);
    }

    // 將收藏的浪浪編號集合寫入偏好設定的方法(務必要加上.clear()，否則在重啟後會遺失數據)
    private void saveFavoriteStrayIDs(HashSet<String> favoriteStrayIDs) {
        sharedPreferences.edit()
                .clear()
                .putStringSet(FAVORITE_STRAY_IDS_PREFERENCE_KEY,favoriteStrayIDs)
                .commit();
    }

    private void shareStrayInfo() {
        // 打包認養資訊
        String shareStrayInfoText = "我在浪浪回家APP看到一隻可愛的浪浪！\n *浪浪編號：" + strayIDTextView.getText() +
                                    "\n *浪浪類型：" + strayKindTextView.getText() +
                                    "\n *浪浪性別：" + strayGenderTextView.getText() +
                                    "\n *浪浪體型：" + strayBodyTypeTextView.getText() +
                                    "\n *浪浪毛色：" + strayClourTextView.getText() +
                                    "\n *浪浪年紀：" + strayAgeTextView.getText() +
                                    "\n *是否絕育：" + straySterilizationTextView.getText() +
                                    "\n *是否施打疫苗：" + strayBacterinTextView.getText() +
                                    "\n *收容所名稱：" + strayShelterNameTextView.getText() +
                                    "\n *收容所電話：" + strayShelterPhoneTextView.getText() +
                                    "\n *收容所地址：" + strayShelterAddressTextView.getText() +
                                    "\n *浪浪照片：" + stray.getAnimalPhoto() + "\n" +
                                    "\n *浪浪回家APP：https://itunes.apple.com/tw/app/id1212078175";

//        Uri strayPhotoUri = Uri.parse(stray.getAnimalPhoto());
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareStrayInfoText);
        // 判斷是否有照片網址，決定setType
//        if (strayPhotoUri != null) {
//            shareIntent.putExtra(Intent.EXTRA_STREAM, strayPhotoUri);
//            shareIntent.setType("image/*");
//        } else {
            shareIntent.setType("text/plain");
//        }
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "請選擇要用來分享浪浪領養資訊應用程式"));
    }

    // 自訂撥號的的警告視窗類別，並實作點擊介面
    public static class CallPhoneAlert extends DialogFragment implements DialogInterface.OnClickListener {
        public CallPhoneAlert() {
        }

        @Override
        // 建立警告視窗元件
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog callPhoneAlert = new AlertDialog.Builder(getActivity())
                    .setIcon(R.drawable.dialog_icon)
                    .setTitle("要撥打電話嗎？")
                    .setMessage("您確定要撥打電話到 " + strayShelterNameTextView.getText() + "(" +
                                strayShelterPhoneTextView.getText() + ")嗎？")
                    .setPositiveButton("撥號", this)
                    .setNegativeButton("取消", this)
                    .create();
            return  callPhoneAlert;
        }

        // 警告視窗的點擊事件
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    // 進行撥號
                    String shelterPhone = (String) strayShelterPhoneTextView.getText();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + shelterPhone));
                    startActivity(intent);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.cancel();
                    break;
                default:
                    break;
            }
        }
    }
}
